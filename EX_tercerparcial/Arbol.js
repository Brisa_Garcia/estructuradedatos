class Arbol {
    constructor() {
        this.nodoPadre = "";
        this.nodos = [];
    }


    NodoPadre(padre, nivel, posicion, valor, nombre) {
        let nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        this.nodos.push(nodo);
        return nodo;
    }

    Nodo(padre, nivel, posicion, valor, nombre) {
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        this.nodos.push(nodo);
        return nodo;
    }

    agregarNodo(valor, nombre) {
        let nodo = new Nodo(null, null, null, valor, nombre);

        if (this.nodos.length == 1) {
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'hIzq';
            else
                nodo.posicion = 'hDer';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 2) {
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'hIzq';
            else
                nodo.posicion = 'hDer';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 3) {
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'hIzq';
            else
                nodo.posicion = 'hDer';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 4) {
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'hIzq';
            else
                nodo.posicion = 'hDer';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 5) {
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'hIzq';
            else
                nodo.posicion = 'hDer';
            this.nodos.push(nodo);
        }
    }
    buscarNodosPadre(padre){
        let nodosEncontrados = [];
        for (let x = 0; x<this.nodos[x];x++){
            if (this.nodos[x].padre == padre)
                nodosEncontrados.push(this.nodos[x]);
        }
        return nodosEncontrados;
    }
}