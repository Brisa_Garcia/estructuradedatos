class Nodo {
    constructor(padre = null, nivel, posicion, valor, nombre) {
        this.padre = padre;
        this.nivel = nivel;
        this.posicion = posicion;
        this.valor = valor
        this.nombre = nombre;
    }
    hIzq(padre, nivel, posicion, valor, nombre)
    {
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        return nodo;
    }
    hDer(padre, nivel, posicion, valor, nombre)
    {
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        return nodo;
    }

}