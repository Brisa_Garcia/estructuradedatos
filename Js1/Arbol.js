class Arbol {

    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = 3;
        this.busquedaElemento;
        this.buscarNodos = [];
        this.caminoNodo = " ";
        this.sumaDeCaminos = " ";
    }

    agregarNodoPadre(){
        var nodo = new Nodo(null,null,"+",100);
        return nodo;
    }

    agregarNodo(pade,nodoPadre,posicion,nombre,valor){
        var nodo = new Nodo(padre,nodoPadre,posicion,nombre,valor);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }

    buscarValor(buscarElemento,nodo){

        if(nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        return this.busquedaElemento;
    }

    buscarCaminoNodo(nodo){
        if(nodo.padre != null){
            this.caminoNodo = this.caminoNodo + " " + nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre+" "+this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if (nodo.padre != null){
            this.sumaDeCaminos = this.sumaDeCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumaDeCaminos;
    }

}

