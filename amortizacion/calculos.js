const importe = document.getElementById('importe');
const plazo = document.getElementById('plazo');
const tasa = document.getElementById('tasa');
const btn_Calcular = document.getElementById('btn_Calcular');

const tabla= document.querySelector('#datos-tabla tbody')

btn_Calcular.addEventListener('click', () => {
    calcularamortizacion(importe.value, tasa.value, plazo.value);
})

function calcularamortizacion(importe, tasa, plazo)
{
    let fecha=[];
    let fechainicio=Date.now();
    let fechafinal=[];
    let mesinicio=moment(fechainicio);
    let mesfinal=moment(fechainicio);
    let dias=0;

    mesfinal.add(1, 'month');

    let intereses=0, flujo=0, amortizacion=0,periodo=0,iva=0;

    amortizacion=importe/plazo;

    for (let i=1; i<=plazo;i++)
    {
        periodo++;
        fecha[i]=mesinicio.format('DD-MM-YYYY');
        fechafinal[i]=mesfinal.format('DD-MM-YYYY');
        dias=(mesfinal.diff(mesinicio, 'days'));
        mesinicio.add(1, 'month');
        mesfinal.add(1, 'month');
        intereses=parseFloat(((importe*tasa)/360)*dias);
        iva=intereses*.16;
        flujo=amortizacion+intereses+iva;
        importe=parseFloat(importe-amortizacion);

        const row=document.createElement('tr')
        row.innerHTML=`
            
            <td>${periodo}</td>
            <td>${fecha[i]}</td>
            <td>${fechafinal[i]}</td>
            <td>${dias}</td>
            <td></td>
            <td>${importe.toFixed(3)}</td>
            <td></td>
            <td>${amortizacion.toFixed(3)}</td>
            <td>${intereses.toFixed(3)}</td>
            <td>${iva.toFixed(3)}</td>
            <td>${flujo.toFixed(3)}</td>
        `;

        tabla.appendChild(row);
    }
}