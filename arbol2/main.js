var arbol = new Arbol();

arbol.nodoPadre = arbol.agregarNodo(0, "Nodo Principal");

arbol.nodoPadre.hI = arbol.agregarNodo(1,"hoja izquierda");
arbol.nodoPadre.hD = arbol.agregarNodo(1,"hoja derecha");

arbol.nodoPadre.hI.hI = arbol.agregarNodo(2,"hoja izquierda");
arbol.nodoPadre.hI.hD = arbol.agregarNodo(2,"hoja derecha");

arbol.nodoPadre.hI.hD.hI = arbol.agregarNodo(3,"hoja izquierda");
arbol.nodoPadre.hI.hD.hD = arbol.agregarNodo(3,"hoja derecha");

console.log(arbol);