class Arbol {
    constructor() {
        this.nodoPadre = "";
        this.nodos = [];
        //this.nivel =4;
    //  this.buscarNivel=[];
       // this.busquedaElemento;
      //  this.caminoNodo = " ";
     //   this.sumaDeCaminos =0;
    }



    NodoPadre(padre, nivel, posicion, valor, nombre) {
        let nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        this.nodos.push(nodo);
        return nodo;
    }
    Nodo(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
        this.nodos.push(nodo);
        return nodo;
    }
    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNivel.push(nodo.valor);

        if(nodo.hasOwnProperty('hIzq'))
            this.verificarNivelHijos(nodo.hIzq);

        if(nodo.hasOwnProperty('hDer'))
            this.verificarNivelHijos(nodo.hDer);

        return this.buscarNivel;

    }
    buscarNombre(buscarElemento,nodo){

        if(nodo.nombre == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hIzq'))
            this.buscarNombre(buscarElemento,nodo.hIzq);

        if(nodo.hasOwnProperty('hDer'))
            this.buscarNombre(buscarElemento,nodo.hDer);

        return this.busquedaElemento;
    }

    buscarCaminoNodo(nodo){
        if(nodo.padre != null){
            this.caminoNodo = this.caminoNodo + " " + nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre +" "+ this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if (nodo.padre != null){
            this.sumaDeCaminos = this.sumaDeCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumaDeCaminos;
    }

    buscarNodosPadre(padre){
        let nodosEncontrados = [];
        for (let x = 0; x<this.nodos[x];x++){
            if (this.nodos[x].padre == padre)
                nodosEncontrados.push(this.nodos[x]);
        }
        return nodosEncontrados;
    }

    agregarNodo(valor, nombre) {
        let nodo = new Nodo(null,null,null,valor, nombre);
        if (this.nodos.length == 1){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel+1;
           if (nodo.valor < this.nodoPadre.valor)
               nodo.posicion = 'hIzq';
           else
               nodo.posicion = 'hDer';
           this.nodos.push(nodo);
        }

    }
}