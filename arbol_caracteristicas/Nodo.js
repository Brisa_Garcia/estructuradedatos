class Nodo {
    constructor(padre = null, nivel, posicion, valor, nombre) {
        this.padre = padre;
        this.nivel = nivel;
        this.posicion = posicion;
        this.valor = valor
        this.nombre = nombre;
        this.caracterisitcas = {
            "Persona 1": {
                "Rol": "Mamá",
                "Nombre": "Silvia",
                "ApeP": "Garcia",
                "ApeM": "Salazar",
                "Descripción": "Alegre",
                "Gustos": ["Leer", "Cocinar", "Correr", "Ver videos", "Comer"]
            },
            "Persona 2": {
                "Rol": "Papá",
                "Nombre": "Melecio",
                "ApeP": "Garcia",
                "ApeM": "Vazquez",
                "Descripción": "Serio",
                "Gustos": ["Tomar cafe", "Leer", "Series", "Musica", "Viajar"]
            }
        };
    }
        hIzq(padre, nivel, posicion, valor, nombre)
        {
            var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
            return nodo;
        }
        hDer(padre, nivel, posicion, valor, nombre)
        {
            var nodo = new Nodo(padre, nivel, posicion, valor, nombre);
            return nodo;
        }

}