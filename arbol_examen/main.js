
var arbol = new Arbol();
var busqueda = "x";

arbol.nodoPadre= arbol.Nodo(null,0,"Nodo Raiz",36,36);

//--Nodos lado izquierdo
arbol.nodoPadre.hIzq = arbol.Nodo(arbol.nodoPadre,1,"hoja izquierda", 16,16);
arbol.nodoPadre.hIzq.hIzq = arbol.Nodo(arbol.nodoPadre.hIzq, 2, "Hoja izquierda", 8, 8);
arbol.nodoPadre.hIzq.hDer = arbol.Nodo(arbol.nodoPadre.hIzq, 2, "hoja Derecha", 8, 8);
arbol.nodoPadre.hIzq.hIzq.hIzq = arbol.Nodo(arbol.nodoPadre.hIzq.hIzq,3,"Hoja izquierda", 4, "e");
arbol.nodoPadre.hIzq.hIzq.hDer = arbol.Nodo(arbol.nodoPadre.hIzq.hIzq,3,"Hoja derecha", 4, 4);
arbol.nodoPadre.hIzq.hIzq.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hIzq.hIzq.hDer,4,"Hoja izquierda", 2, "n");
arbol.nodoPadre.hIzq.hIzq.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hIzq.hIzq.hDer,4,"Hoja derecha", 2, 2);
arbol.nodoPadre.hIzq.hIzq.hDer.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hIzq.hIzq.hDer.hDer,5,"Hoja izquierda", 1, "o");
arbol.nodoPadre.hIzq.hIzq.hDer.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hIzq.hIzq.hDer.hDer,5,"Hoja derecha", 1, "u");

arbol.nodoPadre.hIzq.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hIzq.hDer,3,"Hoja izquierda",4,"a");
arbol.nodoPadre.hIzq.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hIzq.hDer,3,"Hoja derecha",4,4);
arbol.nodoPadre.hIzq.hDer.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hIzq.hDer.hDer,4,"Hoja izquierda",2,"t");
arbol.nodoPadre.hIzq.hDer.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hIzq.hDer.hDer,4,"Hoja derecha",2,"m");

//--Nodos derecho

arbol.nodoPadre.hDer = arbol.Nodo(arbol.nodoPadre,1,"hoja derecha", 20,20);
arbol.nodoPadre.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hDer,2,"hoja Izquierda", 8,8);
arbol.nodoPadre.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hDer,2,"hoja Derecha", 12,12);
arbol.nodoPadre.hDer.hIzq.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hIzq,3,"hoja Izquierda", 4,4);
arbol.nodoPadre.hDer.hIzq.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hIzq,3,"hoja Derecha", 4,4);
arbol.nodoPadre.hDer.hIzq.hIzq.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hIzq.hIzq,4,"hoja Izquierda", 2,"i");
arbol.nodoPadre.hDer.hIzq.hIzq.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hIzq.hIzq,4,"hoja Derecha", 2,2);
arbol.nodoPadre.hDer.hIzq.hIzq.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hIzq.hIzq.hDer,5,"hoja Izquierda", 1,"x");
arbol.nodoPadre.hDer.hIzq.hIzq.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hIzq.hIzq.hDer,5,"hoja Derecha", 1,"p");

arbol.nodoPadre.hDer.hIzq.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hIzq.hDer,4,"Hoja Izquierda",2,"h");
arbol.nodoPadre.hDer.hIzq.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hIzq.hDer,4,"Hoja Derecha",2,"s");

arbol.nodoPadre.hDer.hDer.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hDer,3,"Hoja Izquierda", 5, 5);
arbol.nodoPadre.hDer.hDer.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hDer,3,"Hoja Derecha", 7, " ");
arbol.nodoPadre.hDer.hDer.hIzq.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hDer.hIzq,4,"Hoja Izquierda", 2, 2);
arbol.nodoPadre.hDer.hDer.hIzq.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hDer.hIzq,4,"Hoja Derecha", 3, "f");
arbol.nodoPadre.hDer.hDer.hIzq.hIzq.hIzq = arbol.Nodo(arbol.nodoPadre.hDer.hDer.hIzq.hIzq,5,"Hoja Izquierda", 1, "r");
arbol.nodoPadre.hDer.hDer.hIzq.hIzq.hDer = arbol.Nodo(arbol.nodoPadre.hDer.hDer.hIzq.hIzq,5,"Hoja Derecha", 1, "l");


//Mostrar Arbol
//console.log(arbol)

//-- Muestra los valores del nivel que desees
var resultado = arbol.verificarNivelHijos(arbol.nodoPadre);
console.log("Nivel: " + arbol.nivel + ": " + arbol.buscarNivel);

//--Muestra en que nivel esta ubicado especificamente un dato
/*var busqueda = arbol.buscarNombre(busqueda, arbol.nodoPadre);
console.log(busqueda);

//--Muestra el camino reocrrido para buscar un nodo
var busqueda = arbol.buscarNombre(busqueda, arbol.nodoPadre);
var caminoNodo = arbol.buscarCaminoNodo(busqueda);
console.log(caminoNodo);*/

//--Muestra la suma del valor que tenga los nodos
/*var busqueda = arbol.buscarNombre(busqueda, arbol.nodoPadre);
var sumarCaminoNodo = arbol.sumarCaminoNodo(busqueda, arbol.nodoPadre);
console.log(sumarCaminoNodo);*/