class Arbol{
    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
      // this.nivel = 1;
   //  this.buscarNivel=[];
        this.busquedaElemento;
        this.caminoNodo = " ";
        this.sumaDeCaminos =0;
    }

    agregarNodoPadre(padre,nivel, posicion, valor, nombre){
        var nodo = new Nodo(padre,nivel, posicion, valor, nombre);
        return nodo;
    }

    agregarNodo(padre,nivel, posicion, valor, nombre) {
        var nodo = new Nodo(padre,nivel, posicion, valor, nombre);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNivel.push(nodo.valor);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNivel;

    }
    buscarValor(buscarElemento,nodo){

        if(nodo.nombre == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        return this.busquedaElemento;
    }
    buscarCaminoNodo(nodo){
        if(nodo.padre != null){
            this.caminoNodo = this.caminoNodo + " " + nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre +" "+ this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if (nodo.padre != null){
            this.sumaDeCaminos = this.sumaDeCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumaDeCaminos;
    }

}
