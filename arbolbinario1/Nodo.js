class Nodo{
    constructor(tipo, valor, nivel, padre, hijoI =0,hijoD=0) {
        this.tipo = tipo;
        this.valor = valor;
        this.nivel = nivel;
        this.hijoI = hijoI;
        this.hijoD = hijoD;

        if (hijoI == 1)
            this.hijoIzq = this.crearHijo("i", 0, nivel+1,this.nivel);

        if (hijoD == 1)
            this.hijoDer = this.crearHijo("d", 0, nivel+1,this.nivel);
    }

    crearHijo(tipo,valor,nivel,padre){
        var crearHijo = new Nodo(tipo,valor,nivel,padre);
        return crearHijo;
    }
}