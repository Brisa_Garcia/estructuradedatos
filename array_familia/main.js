var familia = [];

var per1 = [];
per1.push("Mama");
per1.push("Silvia");
per1.push("Garcia");
per1.push("Salazar");
per1.push("Alegre");

var gustos1 = [];
gustos1.push("Leer");
gustos1.push("Cocinar");
gustos1.push("Correr");
gustos1.push("Ver videos");
gustos1.push("Comer");

per1.push(gustos1);

var per2 = [];
per2.push("Papa");
per2.push("Melecio");
per2.push("Garcia");
per2.push("Vazquez");
per2.push("Serio");

var gustos2 = [];
gustos2.push("Tomar cafe");
gustos2.push("Leer");
gustos2.push("Series");
gustos2.push("Musica");
gustos2.push("Viajar");
per2.push(gustos2);

var per3 = [];
per3.push("Hermana");
per3.push("Lizbeth");
per3.push("Garcia");
per3.push("Garcia");
per3.push("Feliz");

var gustos3 = [];
gustos3.push("Trabajar");
gustos3.push("Bailar");
gustos3.push("Salir");
gustos3.push("Musica");
gustos3.push("Chocolate");
per3.push(gustos3);

var per4 = [];
per4.push("Hermana menor");
per4.push("Sayury");
per4.push("Esquivel");
per4.push("Garcia");
per4.push("Enojona");

var gustos4 = [];
gustos4.push("Maquillarse");
gustos4.push("Arreglarse");
gustos4.push("Tenis");
gustos4.push("Ropa");
gustos4.push("Uñas");
per4.push(gustos4);

var per5 = [];
per5.push("Yo");
per5.push("Brisa");
per5.push("Garcia");
per5.push("Garcia");
per5.push("Tranquila");

var gustos5 = [];
gustos5.push("Tomar te");
gustos5.push("Salir");
gustos5.push("Series");
gustos5.push("Vestidos");
gustos5.push("Comer");
per5.push(gustos2);

familia.push(per1, per2, per3, per4, per5);

console.log(familia);

var Familia ={
    "Persona 1" : {
        "Rol" : "Mamá",
        "Nombre" : "Silvia",
        "ApeP" : "Garcia",
        "ApeM" : "Salazar",
        "Descripción" : "Alegre",
        "Gustos" : ["Leer","Cocinar","Correr","Ver videos","Comer"]
    },
    "Persona 2" : {
        "Rol" : "Papá",
        "Nombre" : "Melecio",
        "ApeP" : "Garcia",
        "ApeM" : "Vazquez",
        "Descripción" : "Serio",
        "Gustos" : ["Tomar cafe","Leer","Series","Musica","Viajar"]
    }
}
console.log(Familia);
