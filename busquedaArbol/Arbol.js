class Arbol{
    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
    }

    agregarNodoPadre(nivel, valor, nombre){
        var nodo = new Nodo(nivel, valor, nombre);
        return nodo;
    }

    agregarNodo(nivel, valor, nombre) {
        var nodo = new Nodo(nivel, valor, nombre);
        return nodo;
    }
}