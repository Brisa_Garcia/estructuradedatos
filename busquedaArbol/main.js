var arbol = new Arbol();
var BuscarNodos = [];
var nivel = 3;

arbol.nodoPadre = arbol.agregarNodo(0, "Nodo Principal", "F");

arbol.nodoPadre.hI = arbol.agregarNodo(1,"hoja izquierda", "B");
arbol.nodoPadre.hD = arbol.agregarNodo(1,"hoja derecha", "G");

arbol.nodoPadre.hI.hI = arbol.agregarNodo(2,"hoja izquierda", "A");
arbol.nodoPadre.hI.hD = arbol.agregarNodo(2,"hoja derecha", "D");

arbol.nodoPadre.hI.hD.hI = arbol.agregarNodo(3,"hoja izquierda", "C");
arbol.nodoPadre.hI.hD.hD = arbol.agregarNodo(3,"hoja derecha", "E");

arbol.nodoPadre.hD.hD = arbol.agregarNodo(2,"hoja derecha", "I");
arbol.nodoPadre.hD.hD.hI = arbol.agregarNodo(3,"hoja izquierda", "H");

vHijos(arbol.nodoPadre)
console.log("Nivel:" + nivel + " : " + BuscarNodos);

function vHijos(nodo){

    if(nodo.nivel == nivel)
    BuscarNodos.push(nodo.nombre);

    if (nodo.hasOwnProperty("hI"))
        vHijos(nodo.hI)

    if (nodo.hasOwnProperty("hD"))
        vHijos(nodo.hD)
}