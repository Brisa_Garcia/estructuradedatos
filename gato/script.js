let turno=0;
const tablero =[];

const btnpulsar = (e, pos) =>{
    turno ++;
    const btn=e.target;
    const color=turno % 2 ? 'LightPink':'Thistle';
    btn.style.backgroundColor=color;
    tablero[pos]=color;
    if(win())alert("FELICIDADES HAZ GANDO "+color);
}

const win = () => {
    if(tablero[0] == tablero[1] && tablero[0] == tablero[2] && tablero[0])
    return true;
    if(tablero[3] == tablero[4] && tablero[3] == tablero[5] && tablero[3])
    return true;
    if(tablero[6] == tablero[7] && tablero[8] == tablero[9] && tablero[6])
    return true;
    if(tablero[0] == tablero[3] && tablero[0] == tablero[6] && tablero[0])
    return true;
    if(tablero[1] == tablero[4] && tablero[1] == tablero[7] && tablero[1])
    return true;
    if(tablero[2] == tablero[5] && tablero[2] == tablero[8] && tablero[2])
    return true;
    if(tablero[0] == tablero[4] && tablero[0] == tablero[8] && tablero[0])
    return true;
    if(tablero[2] == tablero[4] && tablero[2] == tablero[6] && tablero[2])
    return true;

    return false;
}

document.querySelectorAll('button').forEach(
    (obj,i) => obj.addEventListener('click', (e) => btnpulsar(e,i)));

